# README #


### What is this repository for? ###

* Simple cli on STM32 NUCLEO-F401RE through USB serial

### How do I get set up? ###

Use latest CUBE IDE to build it and flash through the on board programmer.
Connect STM32 NUCLEO-F401RE through usb cable.
Open TeraTerm and find stm32 usb serial port, set baud to 115200 hit enter.  
Green LED should flash every second if all OK.  

### How was it written ###
Having fun playing with FreeRtos, c++ and std:string.  
It is missing error checking and some things are not abstracted correctly.  

### To add new commands ###
check the task in CLI.cpp


