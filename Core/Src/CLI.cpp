/*
 * CLI.cpp
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#include "CLI.h"
#include "main.h"
#include "usart.h"
#include "semphr.h"
#include "Uart.h"

#define MAXIMUM_LEN_OF_INPUT_STRING 100

CLI cli;


CLI::CLI()
{
	RxQUEUE = xQueueCreate(MAXIMUM_LEN_OF_INPUT_STRING, 1); //create a queue for character storing
}

CLI::~CLI() {
	// TODO Auto-generated destructor stub
}

void CLI::PushCharacterISR(char ch)
{
	xQueueSendFromISR(cli.RxQUEUE,&ch,NULL);
}

void CLI::PushCharacter(char ch)
{
	xQueueSend(cli.RxQUEUE,&ch,0);
}

//returns 0 if no characters
char CLI::PopCharacter(void)
{
	char Temp = 0;
	xQueueReceive(cli.RxQUEUE, &Temp, 0);
	return Temp;
}


void CLI::SendPromptChar(void)
{
	uint8_t PromptChar = '>';
	Uart2.SendBuffer((uint8_t*)&PromptChar, 1);
}

void CLI::SendNewLineChar(void)
{
	uint8_t Char = '\n';
	Uart2.SendBuffer((uint8_t*)&Char, 1);
}

void CLI::SendReturnChar(void)
{
	uint8_t Char = '\r';
	Uart2.SendBuffer((uint8_t*)&Char, 1);
}

void CLI::SendCRLF(void)
{
	Uart2.SendBuffer((uint8_t*)"\r\n", 2);
}

void CLI::SendRemoveChar(void)
{
	char Space = ' ';
	char BackSpace = '\b';

	//do a trick to remove from terminal
	Uart2.SendBuffer((uint8_t*)&BackSpace, 1);
	Uart2.SendBuffer((uint8_t*)&Space, 1);
	Uart2.SendBuffer((uint8_t*)&BackSpace, 1);
}

void CLI::ProcessQueueIntoString(std::string& FullCommand)
{
	char Char = 0;
	do
	{
		Char = cli.PopCharacter();
		//if backspace process it
		if(Char == '\b')
		{
			//also discard the previous one
			FullCommand.pop_back();
		}
		else
		{
			FullCommand += Char;
		}
	}while(Char != 0);
}

bool CLI::CheckIfAnyCommandExecutes(std::string& FullCommand)
{
	bool CommandFound = false;

	for(int i = 0; i < CommandCount; i++)
	{
		if( CommandsList[i]->ValidateAndExecute(FullCommand) == true)
		{
			CommandFound = true;
			break;
		}
	}

	return CommandFound;
}

void CLI::PrintHelp(void)
{
	Uart2.SendBuffer((uint8_t*)"Valid Commands:",sizeof("Valid Commands:"));
	SendCRLF();
	for(int i = 0; i < CommandCount; i++)
	{
		CommandsList[i]->PrintHelp();
		SendCRLF();
	}
}

void CLI::ExecuteStoredCommand(void)
{
	std::string FullCommand;

	ProcessQueueIntoString(FullCommand);

	bool CommandFound = CheckIfAnyCommandExecutes(FullCommand);

	//print help if not found
	if(CommandFound == false)
	{
		PrintHelp();
	}
}

bool CLI::AddCommand(CLI_Command* NewCommand)
{
	if((CommandCount < MAX_NUM_OF_COMMANDS) && NewCommand != NULL)
	{
		CommandsList[CommandCount++] = NewCommand;
		return true;
	}
	else
	{
		return false;
	}
}


void task_CLI(void const *argument)
{
	int Result = -1;//init with invalid
	TickType_t Timeout = 0xFFFF;

	Uart2.Init(&huart2);

	/* add new commands here*/
	std::string run = "run";
	std::string running = "running";

	std::string config = "config";
	std::string configuring = "configuring";

	CLI_Command CommandRun(run, running);
	CLI_Command CommandConfig(config, configuring);

	//no error checking implemented
	cli.AddCommand(&CommandRun);
	cli.AddCommand(&CommandConfig);

	//start with a prompt char
	cli.SendPromptChar();

	for(;;)
	{
		//block using FreeRtos no load on cpu
		Result = Uart2.ReadByte_OSBlock(Timeout);

		// no character validation at the moment
		if(Result > 0)
		{
			//we have a byte of data
			//backspace
			if(Result == '\b')
			{
			  //discard last character stored
			  cli.SendRemoveChar();
			  //push and process later
			  cli.PushCharacter(Result);
			}
			//return
			else if(Result == '\r')
			{
			  //move to the next line
			  cli.SendCRLF();
			  //whole command received
			  cli.ExecuteStoredCommand();
			  cli.SendCRLF();
			  cli.SendPromptChar();
			}
			//new line
			else if(Result == '\n')
			{
			  /*not sure how to handle this, lets echo it back but not interpret
			   * as it will be problematic for the next command in case \r\n has been sent
			   */
			  //echo
			  Uart2.SendBuffer((uint8_t*)&Result, 1);
			}
			//any other character
			else
			{
			  cli.PushCharacter(Result);
			  //echo
			  Uart2.SendBuffer((uint8_t*)&Result, 1);
			}
		}
		else
		{
		  //no data
		}
	}
}
