/*
 * CLICommand.cpp
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#include "CLICommand.h"
#include "Uart.h" //lets print from here to avoid ownership of the string container

CLI_Command::CLI_Command(std::string& Command, std::string& Response)
{
	this->Command = &Command;
	this->Response = &Response;
}

CLI_Command::~CLI_Command() {
	// TODO Auto-generated destructor stub
}

void CLI_Command::PrintHelp()
{
	Uart2.SendBuffer(Command);
}

bool CLI_Command::ValidateAndExecute(std::string& StringToCheck)
{
	if(StringToCheck.compare(0, Command->length(),*Command) == 0)
	{
		Uart2.SendBuffer(Response);
		std::string Arguments = StringToCheck.substr(Command->length());
		Uart2.SendBuffer(&Arguments);
		return true;
	}
	else
	{
		return false;
	}
}
