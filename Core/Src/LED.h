/*
 * LED.h
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#ifndef SRC_LED_H_
#define SRC_LED_H_

#include "FreeRTOS.h"
#include "task.h"

#ifdef __cplusplus

class LED {
public:
	LED();
	virtual ~LED();
};
#endif /* __cplusplus */



#ifdef __cplusplus
extern "C" {
#endif

extern void task_LED(void const *argument);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LED_H_ */
