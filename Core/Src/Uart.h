/*
 * Uart.h
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#ifndef SRC_UART_H_
#define SRC_UART_H_

#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include <string>
class Uart {
public:
	Uart();
	virtual ~Uart();

	void Init(UART_HandleTypeDef* handle);

	int ReadByte_OSBlock(int TimeoutMs);
	int SendBuffer(uint8_t* Buffer, uint8_t Len);
	int SendBuffer(std::string* String);

	void RxPushDataFromISR(uint8_t data);
	void TxISRRoutine(void);

	uint8_t RxData;
	uint8_t TxData;

private:
	QueueHandle_t RxQueue;
	QueueHandle_t TxQueue;
	bool TxInProgress = false;
	UART_HandleTypeDef* STMUARThandle;
};

extern Uart Uart2;

#ifdef __cplusplus
extern "C" {
#endif

extern void task_UartTX(void *argument);

#ifdef __cplusplus
}
#endif

#endif /* SRC_UART_H_ */
