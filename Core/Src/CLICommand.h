/*
 * CLICommand.h
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#ifndef SRC_CLICOMMAND_H_
#define SRC_CLICOMMAND_H_

#include <string>

class CLI_Command {
public:
	CLI_Command(std::string& Command, std::string& Response);
	CLI_Command(const char* Command, const char* Response);
	virtual ~CLI_Command();

	bool ValidateAndExecute(std::string& StringToCheck);
	void PrintHelp();
private:
	std::string* Command = NULL;
	std::string* Response = NULL;
	const std::string EmptyString = "";
};

#endif /* SRC_CLICOMMAND_H_ */
