/*
 * CLI.h
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#ifndef SRC_CLI_H_
#define SRC_CLI_H_

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "CLICommand.h"

#define MAX_NUM_OF_COMMANDS 10

#ifdef __cplusplus
class CLI
{
public:
	CLI();
	virtual ~CLI();

	void PushCharacterISR(char ch);
	void PushCharacter(char ch);
	char PopCharacter(void);
	void SendPromptChar(void);
	void SendNewLineChar(void);
	void SendReturnChar(void);
	void SendRemoveChar(void);
	void SendCRLF(void);

	void ExecuteStoredCommand(void);
	bool AddCommand(CLI_Command* NewCommand);
private:
	void ProcessQueueIntoString(std::string& FullCommand);
	bool CheckIfAnyCommandExecutes(std::string& FullCommand);
	void PrintHelp(void);
	QueueHandle_t RxQUEUE;
	int CommandCount = 0;
	CLI_Command* CommandsList[MAX_NUM_OF_COMMANDS]; //lets do a static memory allocation for now


};
#endif /* __cplusplus */


#ifdef __cplusplus
extern "C" {
#endif

extern void task_CLI(void const *argument);

#ifdef __cplusplus
}
#endif

#endif /* SRC_CLI_H_ */
