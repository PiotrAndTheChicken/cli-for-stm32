/*
 * Uart.cpp
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */

#include "Uart.h"

#define MAXIMUM_LEN_OF_INPUT_STRING 100

Uart Uart2;

/* Would be nice to abstract this into the class, maybe use the callback registration instead, probably as static function*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == &huart2)
	{
		HAL_UART_Receive_IT(&huart2, &Uart2.RxData, sizeof(Uart2.RxData));
		Uart2.RxPushDataFromISR(Uart2.RxData);
	}
	else
	{
		//not handled
	}
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == &huart2)
	{
		Uart2.TxISRRoutine();
	}
	else
	{
		//not handled
	}
}

Uart::Uart() {
	// TODO checking for errors

}

void Uart::Init(UART_HandleTypeDef* handle)
{
	RxQueue = xQueueCreate(MAXIMUM_LEN_OF_INPUT_STRING, 1); //create a queue for character storing
	TxQueue = xQueueCreate(MAXIMUM_LEN_OF_INPUT_STRING, 1); //create a queue for character storing

	STMUARThandle = handle;
	//init reception
	HAL_UART_Receive_IT(&huart2, &RxData, sizeof(RxData));//init receiver
}

Uart::~Uart() {
	// TODO Auto-generated destructor stub
}

int Uart::ReadByte_OSBlock(int TimeoutMs)
{
	char Temp = 0;
	xQueueReceive(RxQueue, &Temp, TimeoutMs);
	return Temp;
}

int Uart::SendBuffer(uint8_t* Buffer, uint8_t Len)
{
	if(Buffer != NULL) //basic checking
	{
		for(int i = 0; i < Len; i++)
		{
			xQueueSend(TxQueue, &Buffer[i], 0);
		}
		if(TxInProgress == false)
		{
			xQueueReceive(TxQueue, &TxData, 0);
			TxInProgress = true;
			HAL_UART_Transmit_IT(&huart2, &TxData, sizeof(TxData));
		}
		//else interrupt will take care of the new chars
	}
}

int Uart::SendBuffer(std::string* String)
{
	for (char const &c: *String)
	{
		SendBuffer((uint8_t*)&c,1);
	}
}


void Uart::TxISRRoutine(void)
{
	if(xQueueReceiveFromISR(TxQueue, &TxData, NULL) == pdTRUE)
	{
		HAL_UART_Transmit_IT(&huart2, &TxData, sizeof(TxData));
	}
	else
	{
		TxInProgress = false;
	}
}

void Uart::RxPushDataFromISR(uint8_t data)
{
	xQueueSendFromISR(RxQueue,&data,NULL);
}
