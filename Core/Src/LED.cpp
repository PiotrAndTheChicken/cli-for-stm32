/*
 * LED.cpp
 *
 *  Created on: Apr 10, 2021
 *      Author: Szary
 */
#include "main.h"
#include "LED.h"
//#include "task.h"


LED::LED() {
	// TODO Auto-generated constructor stub

}

LED::~LED() {
	// TODO Auto-generated destructor stub
}


void task_LED(void const *argument)
{
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	  vTaskDelay(1000);//wait a sec
  }
}
